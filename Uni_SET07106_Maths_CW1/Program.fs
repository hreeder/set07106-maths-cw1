﻿(*  
    Harry Reeder, Edinburgh Napier University
    Matriculation Number: 40052308
    SET07106 - Maths for Software Engineering
    Coursework 1
    
    File: Program.fs
    File Purpose: Contains logic for each question, contains main program to output when ran from console.

    Note: Research has been done into how to use multiple files, and requires that each file have a namespace
    and contain a module for the code to work as intended. The provided data (for questions 1-8) is contained
    within easy.fs, under the same namespace, and the module 'resource'. This is why any statement in this
    file begins with let <var> = resource.<var> etc.
*)

namespace HReeder.Maths.Coursework

module questions = 
   
    // Question 1 - Give the name of the student with number 30039
    let q1 = resource.students |> List.filter (fun (a,b)->a = 30039) |> List.head |> snd

    // Question 2 - Return the list of student names.
    let q2 = resource.students |> List.map snd

    // Question 3 - Return the list of answers given for question 1
    let q3 = resource.choice
            |> List.map snd
            |> List.map (fun l->l.Head)

    // Question 4 - Return the student number for all students called "BASTIEN"
    let q4 = resource.students
            |> List.filter (fun (a,b)->b.Equals("BASTIEN"))
            |> List.head
            |> fst

    // Question 5 - How many times was "a" the correct answer
    let q5 = resource.answer
            |> List.filter (fun (a)->a.Equals("a"))
            |> List.length

    (*  Question 6 - Give the number of students in group "S3" and the number in "AS"
        Method: Create a list of the second item in the tuple of ID, Programme
        Turn this into a sequence, so that we can create a sequence of unique items from the list
        id is shorthand for fun x->x
        Return this into a List and apply the map to get the second tuple from each set
        This produced the desired output
    *) 
    let q6 = resource.prog
            |> List.map snd
            |> Seq.ofList
            |> Seq.countBy id
            |> List.ofSeq


    (*  Question 7 - Give the score for the sequence alexAns
        Method: Zip the two lists (alex's answers and the correct answer) together
        then filter for any items in the combined list where alex's answer matches the correct
        answer. Then give the length of the resultant list to show the score.
    *)
    let q7 = resource.alexAns
            |> List.zip resource.answer
            |> List.filter (fun(a,b)->a=b)
            |> List.length

    (*  Question 8 - Give the score for each student - show the student number and
        mark out of 20.
        Here we create a new list, keeping the student number in-tact and applying
        the same method as Q7 to the B value from the choice resource.
    *)
    let q8 = resource.choice
            |> List.map (fun(a,b)->
                a,b
                  |> List.zip resource.answer
                  |> List.filter (fun(a,b)->a=b)
                  |> List.length)

    [<EntryPoint>]
    let main args =
        printfn "Harry Reeder, 40052308, Maths CW1 \n \n"
        printfn "=== Easy Questions === \n"
        printfn "Question 1: %A \n" q1
        printfn "Question 2: %A \n" q2
        printfn "Question 3: %A \n" q3
        printfn "Question 4: %A \n" q4
        printfn "Question 5: %A \n" q5
        printfn "\n=== Medium Questions ===\n"
        printfn "Question 6: %A \n" q6
        printfn "Question 7: %A \n" q7
        printfn "Question 8: %A \n" q8

        
        // Return 0. This indicates success.
        0