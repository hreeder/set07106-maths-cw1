(*  
    Harry Reeder, Edinburgh Napier University
    Matriculation Number: 40052308
    SET07106 - Maths for Software Engineering
    Coursework 1
    
    File: easy.fs
    File Purpose: Contains data used in Program.fs for questions 1-8 inclusive.
*)

namespace HReeder.Maths.Coursework

module resource = 

    let students =[
        (30002,"ADRIEN");
        (30021,"Alex");
        (30039,"LEO");
        (30052,"DYLAN");
        (30064,"Julien");
        (30097,"BASTIEN");
        ]
    let prog = [
        (30002,"AS");
        (30021,"S3");
        (30039,"S3");
        (30052,"AS");
        (30064,"S3");
        (30097,"S3");
        ]

    let choice =
        [(30002, ["d"; "a"; "x"; "e"; "b"; "c"; "c"; "a"; "e"; "c"; "a"; "d"; "a"; "c"; "d"; "e"]);
        (30021, ["d"; "d"; "c"; "c"; "c"; "b"; "c"; "a"; "d"; "b"; "d"; "d"; "a"; "e"; "a"; "e"]);
        (30039, ["d"; "a"; "d"; "d"; "c"; "d"; "c"; "d"; "e"; "a"; "a"; "c"; "b"; "d"; "d"; "e"]);
        (30052, ["d"; "b"; "d"; "c"; "b"; "d"; "c"; "a"; "d"; "b"; "a"; "c"; "a"; "e"; "a"; "d"]);
        (30064, ["d"; "a"; "d"; "c"; "a"; "b"; "c"; "a"; "d"; "b"; "d"; "c"; "a"; "b"; "b"; "e"]);
        (30097, ["d"; "a"; "c"; "a"; "c"; "b"; "e"; "a"; "d"; "b"; "d"; "c"; "a"; "e"; "a"; "d"])]

    let answer =
        ["d"; "a"; "d"; "c"; "a"; "b"; "c"; "a"; "d"; "b"; "d"; "c"; "c"; "a"; "c"; "e"]

    let alexAns = choice |> List.tail |> List.head |> snd